var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');

module.exports = {
	entry: './src/index.ts',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public')
	},
	devServer: {
		contentBase: './public'
	},
	module: {
		rules: [{
			test: /\.(scss)$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: ['css-loader', 'sass-loader']
			})
		}, {
			test: /\.tsx?$/,
			use: 'ts-loader',
			exclude: /node_modules/
		}, {
			test: /\.svg$/,
			loader: 'svg-inline-loader'
		}, {
			test: /\.(png|jpg|gif)$/,
			use: {
				loader: 'responsive-loader',
				options: {
					sizes: [300],
					placeholder: true,
					placeholderSize: 50,
					name: 'imagemin-webpack-plugin/[name]-[width].[ext]'
				}
			}
		}, {
			test: /\.html$/,
			use: [{
				loader: 'html-loader',
				options: {
					minimize: true,
					interpolate: true
				}
			}]
		}]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js']
	},
	plugins: [
		new ImageminPlugin({
			test: /\.(png|jpg|gif)$/
		}),
		new FaviconsWebpackPlugin({
			// Your source logo
			logo: './src/img/logo-favicon.png',
			// The prefix for all image files (might be a folder or a name)
			prefix: 'icons-[hash]/',
			// Emit all stats of the generated icons
			emitStats: false,
			// The name of the json containing all favicon information
			statsFilename: 'iconstats-[hash].json',
			// Generate a cache file with control hashes and
			// don't rebuild the favicons until those hashes change
			persistentCache: true,
			// Inject the html into the html-webpack-plugin
			inject: true,
			// favicon background color (see https://github.com/haydenbleasel/favicons#usage)
			background: '#fff',
			// favicon app title (see https://github.com/haydenbleasel/favicons#usage)
			title: 'Bruno Safety COnstruction',

			// which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
			icons: {
				android: true,
				appleIcon: true,
				appleStartup: true,
				coast: false,
				favicons: true,
				firefox: true,
				opengraph: false,
				twitter: false,
				yandex: false,
				windows: true
			}
		}),
		new HtmlWebpackPlugin({
			title: "Bruno Safety Consulting LLC",
			template: './src/index.html'
		}),
		new HtmlWebpackInlineSVGPlugin(),
		new ExtractTextPlugin("styles.css")
	]
};